import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AppliWebVote';
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  authentification() {
    
    this.router.navigate(['/authentification']);
  
  }
  VoirResultat(){
    this.router.navigate(['/resultatVote']);
  
  }
}
