import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthentificationComponent} from './authentification/authentification.component';
import { ResultatVoteComponent } from './resultat-vote/resultat-vote.component';
export const routes: Routes =  [
  { path: 'authentification', component: AuthentificationComponent },
  { path: 'resultatVote', component: ResultatVoteComponent },

];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
