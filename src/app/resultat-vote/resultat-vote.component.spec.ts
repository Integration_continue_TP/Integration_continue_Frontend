import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultatVoteComponent } from './resultat-vote.component';

describe('ResultatVoteComponent', () => {
  let component: ResultatVoteComponent;
  let fixture: ComponentFixture<ResultatVoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultatVoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultatVoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
